from django.db import models

# Модель гравця


class Player(models.Model):
    id_player = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    nick_name = models.CharField(max_length=255)
    date_of_birth = models.DateField()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

# Модель команди


class Team(models.Model):
    id_team = models.AutoField(primary_key=True)
    name_team = models.CharField(max_length=255)
    city_team = models.CharField(max_length=255)
    number_of_players = models.IntegerField()

    def __str__(self):
        return self.name_team


# Проміжна модель гравець-команда


class PlayerTeam(models.Model):
    id_player_team = models.AutoField(primary_key=True)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

# Модель матчу


class Match(models.Model):
    id_match = models.AutoField(primary_key=True)
    date_match = models.DateField()
    number_of_minutes = models.IntegerField()
    status = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.date_match} ({self.status})"

# Модель голу


class Goal(models.Model):
    id_goal = models.AutoField(primary_key=True)
    minute = models.IntegerField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    матч = models.ForeignKey(Match, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.minute}' - {self.player}"

# Модель пасу


class Pass(models.Model):
    id_pass = models.AutoField(primary_key=True)
    minute = models.IntegerField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.minute}' - {self.player}"

# Модель жовтої картки


class Card(models.Model):
    id_card = models.AutoField(primary_key=True)
    minute = models.IntegerField()

    # Тип картки: red або yellow
    TYPE_CHOICES = (
        ('red', 'Red'),
        ('yellow', 'Yellow'),
    )
    type_card = models.CharField(
        max_length=6, choices=TYPE_CHOICES, default='yellow')

    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.type_card} - {self.minute}' - {self.player}"


# Модель турніру


class Tournament(models.Model):
    id_tournament = models.AutoField(primary_key=True)
    name_tournament = models.CharField(max_length=255)
    type_tournament = models.CharField(
        max_length=255, choices=(('cup', 'Cup'), ('championship', 'Championship')))
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return f"{self.name_tournament} ({self.type_tournament})"

# Зв'язок між турніром та командою


class TournamentTeam(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

# Зв'язок між турніром та матчем


class TournamentMatch(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
