from django.http import HttpResponse
from django.shortcuts import render


# def index(request):
#     return HttpResponse('Hello word')


def end_lesson(request):
    return HttpResponse('Good bye!')


def index(request):
    return render(request, "index.html")
