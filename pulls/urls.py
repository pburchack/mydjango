from django.urls import path
from . import views


app_name = 'pulls'

urlpatterns = [
    path('', views.index, name='index'),
    path('bye', views.end_lesson, name='bye'),
]
